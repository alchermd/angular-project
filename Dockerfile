FROM johnpapa/angular-cli

WORKDIR /app

ENV PATH /app/node_modules/.bin:$PATH

COPY package.json ./
RUN npm install --silent

COPY . ./

CMD ["ng", "serve", "--host", "0.0.0.0"]

